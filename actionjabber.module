<?php

/*
 * @file
 * Provides an action to send message using XMPP protocol.
 */

/**
 * Implementation of hook_action_info().
 */
function actionjabber_action_info() {
  return array(
    'actionjabber_sendmessage_action' => array(
      'description' => t('Send Jabber message'),
      'type' => 'system',
      'configurable' => TRUE,
      'hooks' => array(
        'any' => TRUE,
      )
    ),
  );
}

/**
 * Return a form definition so the action can be configured.
 *
 * @see actionjabber_sendmessage_action_validate()
 * @see actionjabber_sendmessage_action_submit()
 * @param $context
 *   Default values (if we are editing an existing action instance).
 * @return
 *   Form definition.
 */
function actionjabber_sendmessage_action_form($context) {
  // Set default values for form.
  global $user;
  $host = url(NULL, array('absolute' => TRUE));
  $host = parse_url($host, PHP_URL_HOST);
  
  if (!isset($context['recipient'])) {
    $context['recipient'] = $user->name . '@' . $host;
  }
  if (!isset($context['username'])) {
    $context['username'] = $user->name;
  }
  if (!isset($context['server'])) {
    $context['server'] = $host;
  }
  if (!isset($context['port'])) {
    $context['port'] = '5222';
  }
  if (!isset($context['timeout'])) {
    $context['timeout'] = '60';
  }
  if (!isset($context['password'])) {
    $context['password'] = '';
  }
  if (!isset($context['message'])) {
    $context['message'] = t('This is a test message!');
  }
  
  
  $form['recipient'] = array(
    '#type' => 'textfield',
    '#title' => t('Recipient'),
    '#default_value' => $context['recipient'],
    //'#maxlength' => '25',
    '#description' => t('Specify the Jabber account you would like to send message.'),
    '#required' => TRUE,
  );

  // XMPP settings
  $form['XMPP'] = array(
    '#type' => 'fieldset',
    '#title' => t('XMPP settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['XMPP']['server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server'),
    '#default_value' => $context['server'],
    //'#maxlength' => '254',
    //'#description' => t('XMPP server.'),
    '#required' => TRUE,
  );
  $form['XMPP']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $context['username'],
    //'#maxlength' => '25',
    // '#description' => t('Username.'),
    '#required' => TRUE,
  );
  $form['XMPP']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Password for account: username@server'),
  );
  $form['XMPP_password'] = array(
    '#type' => 'value',
    '#value' => $context['password'],
  );
  $form['XMPP']['port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => $context['port'],
    //'#maxlength' => '5',
    // '#description' => t('XMPP port.'),
    '#required' => TRUE,
  );
    $form['XMPP']['timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeout'),
    '#default_value' => $context['timeout'],
    //'#maxlength' => '5',
    '#description' => t('Silent time after unsuccessful attempt (min). If this value is small your site may work slowly.'),
    '#required' => TRUE,
  );
  // message components
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => $context['message'],
    '#cols' => '80',
    '#rows' => '5',
    '#description' => t('The message to be displayed to the current user. You may include the following variables: %site_name, %username, %node_url, %node_type, %title, %teaser, %body. Not all variables will be available in all contexts.') . ' ' . t('You can also use %date, %time, %uid.'),
    '#required' => TRUE,
  );
    $form['test'] = array(
    '#type' => 'checkbox',
    '#default_value' => FALSE,
    '#description' => t('Send test message now. May take a long time if XMPP settings is incorrect.'),
  );
    if (!file_exists(drupal_get_path('module', 'actionjabber').'/XMPPHP/XMPP.php')) {
    drupal_set_message(t('Please install XMPPHP: ') . l('The PHP XMPP Library','http://code.google.com/p/xmpphp/'),'error');
  }
  return $form;
}

/**
 * Process actionjabber_sendmessage_action form submissions.
 */
function actionjabber_sendmessage_action_submit($form, &$form_state) {
  // Process the HTML form to store configuration. The keyed array that
  // we return will be serialized to the database.
  $params = array(
    'recipient' => $form_state['values']['recipient'],
    'username'   => $form_state['values']['username'],
    'server'   => $form_state['values']['server'],
    'port' => $form_state['values']['port'],
    'password'   => $form_state['values']['password'],
    'message'   => $form_state['values']['message'],
    'timeout'   => $form_state['values']['timeout'],
  );
  if ( $params['password']==''){
       $params['password'] = $form_state['values']['XMPP_password'];
  }
  // Send a test message
  if ($form_state['values']['test'] == TRUE) {
    variable_set("actionjabber_failedtime", 0);
    actionjabber_sendmessage_action(NULL, $params);
  }
  return $params;
}

/**
 * Implementation of a configurable Drupal action. Sends a message to specified address.
 */
function actionjabber_sendmessage_action($object, $context) {
  // Check timeout if last attempt was unsuccessful.
  $failedtime = variable_get("actionjabber_failedtime", 0);
  if (time() < 60 * (int) $context['timeout'] + (int) $failedtime) {
    watchdog('error', ("Message was not sent. Waiting after unsuccessful attempt."));
    return FALSE;
  };
  if (!file_exists(drupal_get_path('module', 'actionjabber').'/XMPPHP/XMPP.php')) {
    // drupal_set_message(t('Please install XMPPHP: ') . l('The PHP XMPP Library','http://code.google.com/p/xmpphp/'),'error');
    return FALSE;
  }
  // Get context.
  global $user;
  switch ($context['hook']) {
    case 'nodeapi':
      $node = $context['node'];
      break;
    case 'comment':
      $comment = $context['comment'];
      $node = node_load($comment->nid);
      break;
    case 'user':
      if (isset($context['node'])) {
        $node = $context['node'];
      }
      break;
    default:
      // We are being called directly.
      $node = $object;
      variable_set("actionjabber_failedtime", 0);
  }
  
    $variables = array(
      '%date' => date("Y-m-d"),
      '%time' => date("H:i:s"),
      '%uid' => $user->uid,
      '%site_name' => variable_get('site_name', 'Drupal'),
      '%username' => $user->name,
      '%node_url' => url('node/' . $node->nid, array('absolute' => TRUE)),
      '%node_type' => node_get_types('name', $node),
      '%title' => filter_xss($node->title),
      '%teaser' => filter_xss($node->teaser),
      '%body' => filter_xss($node->body)
  );
   $message = strtr($context['message'], $variables);
  
  //include 'XMPPHP/XMPP.php';
  include_once drupal_get_path('module', 'actionjabber').'/XMPPHP/XMPP.php';

  $conn = new XMPPHP_XMPP($context['server'], $context['port'], $context['username'], $context['password'], 'xmpphp', $context['server'], $printlog=false, $loglevel=XMPPHP_Log::LEVEL_INFO);

try {
    $success=true;
    $conn->connect();
    $conn->processUntil('session_start');
    $conn->presence();
    $conn->message($context['recipient'], $message);
    $conn->disconnect();
} catch(XMPPHP_Exception $e) {
    $success=false;
    variable_set("actionjabber_failedtime", time());
    drupal_set_message(t('ActionJabber: Unable to send message.'),'error');
    watchdog('error', ('ActionJabber: Unable to send message to %recipient. Waiting for %timeout min. (%xmppmessage)'), array('%xmppmessage' => $e->getMessage(), '%recipient' => $context['recipient'], '%timeout' => $context['timeout']));
}
   if ($success) {watchdog('actions', "ActionJabber: Message sent successfuly.");};
}
